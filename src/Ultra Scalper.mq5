//+------------------------------------------------------------------+
//|                                                Ultra Scalper.mq5 |
//|                                                    The Programer |
//|                                              theprogramer.com.br |
//+------------------------------------------------------------------+
#property copyright "The Programer"
#property link      "theprogramer.com.br"
#property version   "1.00"

#include <Trade\Trade.mqh>
#include <Trade\OrderInfo.mqh>
CTrade ExtTrade;

//--- input parameters
input string section0 = "---Horários de abertura e encerramento---";
input int startHour = 10;
input int finishHour = 16;
input string section1 = "---Distância da Média---";
input int op1 = 1000;
input int op2 = 1000;
input int op3 = 1000;
input int op4 = 1000;
input int op5 = 1000;
input int op6 = 1000;
input int op7 = 1000;
input int op8 = 1000;
input string section2 = "---Take Profit---";
input int takeProfit = 1000;
input string section3 = "---Stop Loss---";
input int sl1 = 1600;
input int sl2 = 1600;
input int sl3 = 1600;
input int sl4 = 1600;
input int sl5 = 1600;
input int sl6 = 1600;
input int sl7 = 1600;
input int sl8 = 1600;
input string section4 = "---Volume de Entrada---";
input int sz1 = 2;
input int sz2 = 4;
input int sz3 = 8;
input int sz4 = 16;
input int sz5 = 40;
input int sz6 = 80;
input int sz7 = 180;
input int sz8 = 220;
input string section5 = "---Perda/Ganhos Máx p/d---";
input int maxLoss = 3;
input int maxWin = 3;
input int      MAPeriod=1000;
input int      Step=500;
input int      MaxStep=4;
input long     Magic=777777;
//---
int    ExtHandle=0;
double      MA[];
double      target;
int         MA_handle;
int         CURRENT_MODE;
int         operations[8];
int         sizes[8];
int         tps[8];
int         sls[8];
bool        pos[8];
int         lossCount = 0;
int         winCount = 0;
int         dayWinCount = 0;
int         dayLossCount = 0;
int step = 0;
//+------------------------------------------------------------------+
//| Init                                                             |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- Init iMA
   MA_handle=iMA(NULL,0,MAPeriod,0,MODE_EMA,PRICE_CLOSE);
   CURRENT_MODE=0;
   operations[0] = op1;
   operations[1] = op2;
   operations[2] = op3;
   operations[3] = op4;
   operations[4] = op5;
   operations[5] = op6;
   operations[6] = op7;
   operations[7] = op8;
   tps[0] = takeProfit; // 1000
   tps[1] = (takeProfit + op2 / (sz1 + sz2)) + 1; // 1000 + 1000 / (2 + 4) = 334
   tps[2] = (takeProfit + op2 + op3 / (sz1 + sz2 + sz3)) + 1; // 1000 + 1000 + 1000 / (2 + 4 + 8) = 215
   tps[3] = (takeProfit + op2 + op3 + op4 / (sz1 + sz2 + sz3 + sz4)) + 1; // 1000 + 1000 + 1000 + 1000 / (2 + 4 + 8 + 16) = 143
   tps[4] = (takeProfit + op2 + op3 + op4 + op5 / (sz1 + sz2 + sz3 + sz4 + sz5)) + 1; // 1000 + 1000 + 1000 + 1000 + 1000 / (2 + 4 + 8 + 16 + 40) = 84
   tps[5] = (takeProfit + op2 + op3 + op4 + op5 + op6 / (sz1 + sz2 + sz3 + sz4 + sz5 + op6)) + 1; // 1000 + 1000 + 1000 + 1000 + 1000 + 1000 / (2 + 4 + 8 + 16 + 40 + 80) = 41
   tps[6] = (takeProfit + op2 + op3 + op4 + op5 + op6 + op7 / (sz1 + sz2 + sz3 + sz4 + sz5 + op6 + op7)) + 1; // 1000 + 1000 + 1000 + 1000 + 1000 + 1000 + 1000 / (2 + 4 + 8 + 16 + 40 + 80 + 180) = 41
   tps[7] = (takeProfit + op2 + op3 + op4 + op5 + op6 + op7 + op8 / (sz1 + sz2 + sz3 + sz4 + sz5 + op6 + op7 + op8)) + 1; // 1000 + 1000 + 1000 + 1000 + 1000 + 1000 + 1000 + 1000 / (2 + 4 + 8 + 16 + 40 + 80 + 180 + 220) = 25
   sls[0] = sl1;
   sls[1] = sl2;
   sls[2] = sl3;
   sls[3] = sl4;
   sls[4] = sl5;
   sls[5] = sl6;
   sls[6] = sl7;
   sls[7] = sl8;
   sizes[0] = sz1;
   sizes[1] = sz2;
   sizes[2] = sz3;
   sizes[3] = sz4;
   sizes[4] = sz5;
   sizes[5] = sz6;
   sizes[6] = sz7;
   sizes[7] = sz8;
   ArrayFill(pos,0,8,false);
   return(0);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//--- Open orders if not exists
    CopyBuffer(MA_handle,0,0,10,MA);
    ArraySetAsSeries(MA,true);
    MqlTick Latest_Price;
    MqlDateTime tc;
    SymbolInfoTick(Symbol() ,Latest_Price);
    if (PositionsTotal() == 0) target = MA[0];
    bool Result = false;
    TimeCurrent(tc);
    if(tc.hour==startHour && PositionsTotal() == 0)
      {
         dayLossCount = lossCount;
         dayWinCount = winCount;         
      }
    if(tc.hour>=startHour && tc.hour <= finishHour)
      {
        if (countLosses() < maxLoss && countWinning() < maxWin)
        {
          for(step=0;step<8; step++) 
            { 
               if (CURRENT_MODE == step) {
               if (Latest_Price.bid > target + (_Point*operations[step]) && !pos[step]) {
                 Result = ExtTrade.PositionOpen(_Symbol, ORDER_TYPE_SELL, sizes[step], Latest_Price.bid, Latest_Price.bid + (_Point*sls[step]), Latest_Price.bid - (_Point*tps[step]), NULL);
                   if (Result) target = Latest_Price.bid;
               } else if (Latest_Price.ask < target - (_Point*operations[step]) && !pos[step]) {
                   Result = ExtTrade.PositionOpen(_Symbol, ORDER_TYPE_BUY, sizes[step], Latest_Price.ask, Latest_Price.ask - (_Point*sls[step]), Latest_Price.ask + (_Point*tps[step]), NULL);
                  if (Result) target = Latest_Price.ask;
               }
               if (Result) {
                   CURRENT_MODE += 1;
                  pos[step]=true;
                  Result = false;
                  break;
               }
               }
            }       
        }
      }
    else
      {
         CloseAllPositions();
      }
    if (PositionsTotal() == 0) {
    //  CloseAllPending();
      CURRENT_MODE=0;
      ArrayFill(pos,0,8,false);
      updateCounters();
    }
  }
//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer()
  {
//---

  }
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
//---
    //Print("Entrou Aqui!");
  }

void CloseAllPositions()
  {
     for(int i=PositionsTotal()-1; i>=0; i--)
       {
          ulong ticket=PositionGetTicket(i);
          ExtTrade.PositionClose(ticket);   
       }    
  }

void CloseAllPending()
{
  CTrade pending;
   int o_total=OrdersTotal();
   for(int j=o_total-1; j>=0; j--)
   {
      ulong o_ticket = OrderGetTicket(j);
      if(o_ticket != 0)
      {
       // delete the pending order
       pending.OrderDelete(o_ticket);
       //Print("Pending order deleted sucessfully!");
    }
  } 
}

void updateCounters()
{
  if (HistorySelect(0, INT_MAX))
  {
    lossCount = 0;
    winCount = 0;
    for (int i = HistoryDealsTotal() - 1; i >= 0; i--)
    {
      const ulong Ticket = HistoryDealGetTicket(i);
      if(/*(HistoryDealGetInteger(Ticket, DEAL_MAGIC) == MagicNumber) &&*/ (HistoryDealGetString(Ticket, DEAL_SYMBOL) == Symbol()))
      {
        if (HistoryDealGetDouble(Ticket, DEAL_PROFIT) < 0)
          lossCount++;
        else
          winCount++;
      }
    }
  }
   Print("Perdas: " + IntegerToString(lossCount));
   Print("Ganhos: " + IntegerToString(winCount));
}

int countLosses()
{
   return lossCount - dayLossCount;
}

int countWinning()
{
   return winCount - dayWinCount;
}
//+------------------------------------------------------------------+
